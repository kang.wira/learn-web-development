Untuk memberikan style pada bagian tampilan halaman web, kita menambahkan "style". Umumnya penambahan style
ini memiliki tiga metode yaitu:
- inline styling
- internal styling
- external styling

1. Inline styling adalah metode memberikan style dengan cara menambahkan atribut "style" pada tag yang
diinginkan. Contoh <p style="color : red;">. Di dalam contoh ini, kita menambahkan atribut style
pada tag "p". Setiap style diakhiri dengan titik koma (;).

2. Internal styling adalah metode memberikan style dengan cara menambahkan tag style pada bagian head dalam 
file HTML. Tag style berisi script CSS yang seperti contoh di bawah ini

p {
    color: red;
}

Pada script ini, kita memberikan style pada tag "p" dengan color "red". Jika kita ingin menambahkan lebih dari
satu style, kita pisah dengan koma (,) seperti berikut:

p {
    color: red;
},
h1 {
    color: blue;
}

3. External styling adalah metode memberikan style dengan cara menyambungkan CSS script terpisah ke dalam file HTML
yang diinginkan. CSS script tidak perlu dipisah dengan koma seperti pada internal. Script CSS disambungkan dengan menambahkan tag ini:
<link rel="stylesheet" href="styles.css">
- tag link merupakan tag yang mendefinisikan sambungan dari dokumen eksternal
- atribut rel merupakan atribut yang berisi "relationship" link tersebut. Dalam contoh ini, relationnya adalah
sebuah stylesheet
- href merupakan atribut untuk menyimpan informasi lokasi dokumen / URL. Untuk dokumen, sementara ini kita pake
relative path dulu aja yang berarti path dari directory tempat dokumen HTMLnya disimpan.
Contoh:
- href="styles.css" artinya, filenya bernama styles.css dan terletak di satu directory/folder tempat file HTMLnya berada
- href="sebuah_folder/styles.css" artinya, filenya bernama styles.css dan terletak di folder "sebuah_folder"

=================================================================================================================
Sedikit info tentang mengakses file lokal dan sedikit info relative path

Tadi sebelumnya sempat kita singgung href sebagai referensi dokumen.
Untuk mengakses dokumen lokal, kita dapat menggunakan relative path. Relative path 
mengacu pada direktori pada file yang dibuka. Dalam directory terdapat parent dan
child. Berikut contoh directory untuk gambaran parent dan child directory:

alpha
--beta
----gamma
----epsilon
--delta

Anggaplah ilustrasi di atas seperti kita berada di sebuah file explorer.
Dalam ilustrasi ini, "alpha" memiliki folder "beta" dan "delta" dan folder
"beta" memiliki folder "gamma" dan "epsilon". Dari ilustrasi ini, informasi yang
bisa kita peroleh adalah :
- alpha memiliki child directory beta dan delta (alpha menjadi parent directory)
- beta memiliki child directory gamma dan epsilon (beta menjadi parent directory).

Dalam penulisan relative path ke href, ketentuan penulisan referensi sebagai berikut, dengan 
asumsi kita berada di folder "beta":
- untuk mengakses file yang berada di dalam direktori yang sama, kita tinggal membuka file tersebut.
contoh : <link rel="stylesheet" href="sebuah-file.jenisfile">sesuatu</a>

- untuk mengakses file yang berada di dalam child directory (misal dari beta ke gamma), sertakan nama folder
sesuai urutan directory ke file tersebut.
contoh : <link rel="stylesheet" href="gamma/sebuah-file.jenisfile">sesuatu</a>

- untuk mengakses file yang berada di dalam parent directory, sertakan "../", notasi untuk kembali ke
parent directory. Jumlah ".." tergantung dari level parent yang ingin dituju
contoh :  <link rel="stylesheet" href="../sebuah-file.jenisfile">sesuatu</a>
kalau mau cari parent dari si parent ya tinggal tambahkan "../" lagi di depan href barusan

============================================================================================================