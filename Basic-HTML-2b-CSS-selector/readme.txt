Dalam HTML, kita bisa membuat id dan class untuk mengidentifikasi komponen mana 
yang akan diberikan style.

new terms :
- <div>
    <div> merupakan sebuah tag untuk meletakkan section dari sebuah file HTML, sehingga
    lebih mudah di-manage untuk pemberian style
- id
    id merupakan sebuah atribut untuk satu element yang unik. Dalam satu page, tidak boleh
    ada lebih dari satu item dengan id yang sama
- class
    class merupakan sebuah atribut untuk lebih dari satu element dalam file HTML anda. Satu tag bisa
    diberikan lebih dari satu class.

CSS Selector

Salah satu pengaplikasian id dan class ini adalah ketika kita ingin memberikan style baik internal atau
external. CSS Selector membuat kita bisa memberikan style yang tepat pada elemen2 dalam halaman web kita.
Contoh penggunaan yang umumnya dilakukan sebagai berikut :

- #alpha{
        //style2 yang ingin diberikan
    }
    notasi ini berarti style yang ada di dalam kurung kurawal (kurung keriting),
    diberikan untuk tag dengan id "alpha"

- .beta{
        //style2 yang ingin diberikan
    }
    notasi ini berarti style yang ada di dalam kurung kurawal (kurung keriting),
    diberikan untuk tag dengan class "beta"

- h1{
        //style2 yang ingin diberikan
    }
    notasi ini berarti style yang ada di dalam kurung kurawal (kurung keriting),
    diberikan untuk tag "h1"

- #alpha .beta{
        //style2 yang ingin diberikan
    }
    notasi ini berarti style yang ada di dalam kurung kurawal (kurung keriting),
    diberikan untuk tag dengan class "beta" yang berada di dalam tag dengan id "alpha".

- #beta, #gamma{
        //style2 yang ingin diberikan
    }
    notasi ini berarti style yang ada di dalam kurung kurawal (kurung keriting),
    diberikan untuk tag dengan class "beta" atau class "gamma".

Secara umum, CSS yang digunakan seperti ini. Jika ingin mencari lebih dalam, bisa ke 
https://www.w3schools.com/cssref/css_selectors.asp