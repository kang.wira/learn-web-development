Untuk membuat tabel dalam file HTML, kita menggunakan tag <table>.

Struktur dasar <table> dalam file HTML sebagai berikut

<table>
    <tr>
        <td> sesuatu </td>
    <tr>
</table>

Penjelasan :
- <table> : merupakan wadah untuk isi dari tabel yang akan anda buat

- <tr> : merupakan tag untuk baris pada tabel anda

- <td> (bisa juga diganti <th> untuk kepala tabel) : merupakan tag untuk data pada cell di tabel
    (kalau yang pernah pakai Microsoft Excel, ya anggap aja kayak 1 kotak kecil di situ)

========================================================================================================
Rowspan dan Colspan

rowspan dan colspan merupakan atribut yang bertujuan untuk menggabungkan baris atau kolom pada suatu 
data cell

Contoh penggunaan rowspan:

<td rowspan="2"> sesuatu </td>
Pada contoh di atas, td digabungkan dengan cell sebanyak 2 baris ke bawah (sudah termasuk td itu sendiri)

Contoh penggunaan colspan:
<td colspan="2"> sesuatu </td>
Pada contoh di atas, td digabungkan dengan cell sebanyak 2 kolom ke kanan (sudah termasuk td itu sendiri)