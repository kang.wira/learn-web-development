Untuk menambahkan gambar dalam dokumen HTML, kita dapat menggunakan tag <img>.
contoh penggunaan tag <img> adalah sebagai berikut:
<img src="direktori_gambar.format_gambar"/>

Beberapa atribut dasar yang umumnya digunakan dalam tag <img>
- src : merupakan atribut wajib sebagai referensi lokasi gambar yang
    ingin ditampilkan. src bisa berupa URL dari web, absolute path, atau relative path (lihat bab 2 atau 3).

- height : merupakan atribut untuk mengatur tinggi dari gambar

- width : merupakan atribut untuk mengatur lebar dari gambar

- alt : merupakan atribut sebagai judul gambar alternatif jika gambar gagal dimuat ke dalam halaman web

=========================================
Menjadikan gambar sebagai link
=========================================

Anda bisa menjadikan gambar anda sebagai link menuju halaman HTML lain dengan cara memasukkan tag <a> ke dalam
tag <img> anda. 

Contoh :
<a href = "url_tujuan">
    <img src = "directory_gambar"/>
</a>