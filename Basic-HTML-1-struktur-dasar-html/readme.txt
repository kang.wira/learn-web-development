Struktur dasar sebuah file HTML memiliki beberapa tag (tag itu yang kayak <HTML>, <head>, dst.) seperti

1.  <html> atau <!DOCTYPE html> (sama aja, cuma dengan !DOCTYPE, kita memberikan info versi dan tipe HTML
    sehingga membantu browser untuk nge-load si file HTML tsb). adalah tag yang berfungsi sebagai wadah 
    kode2 file HTML ditulis.

2.  <head> adalah tag untuk wadah buat komponen-komponen pendukung file dan tidak ditampilkan ke dalam
    browser. contohnya seperti tag <title>

3.  <title> adalah tag yang berisi judul dari file/halaman. judulnya entar muncul di tab browser

4.  <body> adalah tag yang berisi komponen-komponen konten dalam file HTML

tag lain yang digunakan dalam file contoh adalah

1.  <h1> merupakan heading tag, sebuah tag yang umumnya digunakan untuk heading konten halaman website.
    heading tag ini terdapat dari <h1> hingga <h6> dengan ukuran teks yang berbeda-beda (semakin besar value h, semakin kecil).

2.  <p> merupakan tag paragraf. 