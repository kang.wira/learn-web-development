Untuk membuat hyperlink, kita akan membuat sebuah anchor tag (<a>).
Hyperlink bertujuan untuk membuat sebuah halaman dapat tersambung ke halaman lain

Kita membuat hyperlink dengan cara
<a href="link tujuan"> teks hyperlink </a>
href merupakan atribut untuk referensi dokumen atau halaman tujuan.

=================================================================================================================
Sedikit pengingat tentang mengakses file lokal dan sedikit info relative path

Pada bab HTML Basic 2 (styling), kita pernah membahas href sebagai referensi dokumen.
Untuk mengakses dokumen lokal, kita dapat menggunakan relative path. Relative path 
mengacu pada direktori pada file yang dibuka. Dalam directory terdapat parent dan
child. Berikut contoh directory untuk gambaran parent dan child directory:

alpha
--beta
----gamma
----epsilon
--delta

Anggaplah ilustrasi di atas seperti kita berada di sebuah file explorer.
Dalam ilustrasi ini, "alpha" memiliki folder "beta" dan "delta" dan folder
"beta" memiliki folder "gamma" dan "epsilon". Dari ilustrasi ini, informasi yang
bisa kita peroleh adalah :
- alpha memiliki child directory beta dan delta (alpha menjadi parent directory)
- beta memiliki child directory gamma dan epsilon (beta menjadi parent directory).

Dalam penulisan relative path ke href, ketentuan penulisan referensi sebagai berikut, dengan 
asumsi kita berada di folder "beta":
- untuk mengakses file yang berada di dalam direktori yang sama, kita tinggal membuka file tersebut.
contoh : <a href="sebuah-file.jenisfile">sesuatu</a>

- untuk mengakses file yang berada di dalam child directory (misal dari beta ke gamma), sertakan nama folder
sesuai urutan directory ke file tersebut.
contoh : <a href="gamma/sebuah-file.jenisfile">sesuatu</a>

- untuk mengakses file yang berada di dalam parent directory, sertakan "../", notasi untuk kembali ke
parent directory. Jumlah ".." tergantung dari level parent yang ingin dituju
contoh :  <a href="../sebuah-file.jenisfile">sesuatu</a>
kalau mau cari parent dari si parent ya tinggal tambahkan "../" lagi di depan href barusan

============================================================================================================
