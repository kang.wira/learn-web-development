terdapat dua cara untuk membuat list/daftar di halaman HTML anda: Ordered list, dan unordered list.
Ordered list merupakan list yang diberi nomor atau urutan, unordered list merupakan list yang tidak
diberi nomor atau urutan.

Ordered list dan unordered list kita membuat "wadah" terlebih dahulu dengan

<ol>
  *isi list*
</ol> untuk ordered list

dan 
<ul>
  *isi list*
</ul> untuk unordered 

=========================================================================================
List styling
=========================================================================================
kita bisa mengganti style "kepala" dari list kita.

untuk style pada unordered list <ul>, berikan style list-style-type pada tag <ul> anda
contoh: <ul style="list-style-type:circle;"> akan menghasilkan kepala list dengan bentuk
lingkaran kosong.

terdapat tiga style dalam list-style-type:
- circle -> bentuk lingkaran kosong
- square -> bentuk kotak terisi (maksudnya dihitamkan)
- none -> tidak ada kepala list

untuk style pada ordered list <ol>, berikan atribut type pada tag <ol> anda
contoh: <ol type="A"> akan menghasilkan kepala list dengan huruf abjad kapital (A, B, C, D, dst)

pada umumnya, type yang dapat digunakan adalah
- angka numerik <ol type="1">
- angka romawi <ol type="I"> untuk angka romawi kapital (I, II, III, dst) atau <ol type="i"> untuk angka romawi 
"kecil" (i, ii, iii, dst)
- huruf abjad <ol type="A"> untuk huruf kapital (A, B, C, dst) atau <ol type="a"> untuk huruf abjad "kecil"
(a, b, c, d)

==============================================================================================
List Bertingkat (Nested List)
=============================================================================================
kita juga bisa membuat list bertingkat (jadi seperti list di dalam list, kayak subbab dalam daftar isi misal)
kita bisa membuat list seperti ini
contoh

<ol>
  <li>Bab 1
      <ul>
          <li>Subbab 1.1</li>
          <li>Subbab 1.2</li>
      </ul>
  </li>
  <li>Bab 2</li>
  <li>Bab 3
      <ul>
          <li>Subbab 3.1</li>
          <li>Subbab 3.2</li>
      </ul>
  </li>
</ol>

untuk jenis list tidak tergantung dengan list asal. misal kalau listnya ordered list, maka boleh-boleh saja
membuat list anaknya sebagai unordered list.